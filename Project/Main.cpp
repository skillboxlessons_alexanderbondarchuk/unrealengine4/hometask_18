#include <iostream>

template <typename T>
class Stack {
private:
	T* data;
	int _top, _size;

	void Resize() {
		int newSize = _size * 2;
		T* newArray = new T[newSize];

		for (int i = 0; i < _size; i++) {
			newArray[i] = *(data + i);
		}
		
		data = newArray;
		_size = newSize;

		//delete newArray;
		newArray = nullptr;
	}
public :
	Stack(int size = 1) : _size(size) {
		data = new T[_size]();
		_top = -1;
	}

	T* Pop() {
		return &data[_top--];
	}

	void Push(T a) {
		if (_top >= _size - 1) {
			Resize();
		}

		data[++_top] = a;
	}

	void Show() {
		for (int* a = data; a < data + _size; a++) {
			std::cout << *a << std::endl;
		}
	}
};

void main()
{
	std::cout << "Stack 1" << std::endl;
	Stack<int>* s = new Stack<int>();
	s->Push(1);
	s->Push(6);
	s->Push(14);
	s->Push(93);

	std::cout << *s->Pop() << std::endl;
	std::cout << *s->Pop() << std::endl;
	std::cout << *s->Pop() << std::endl;
	std::cout << *s->Pop() << std::endl << std::endl;

	std::cout << "Stack 2" << std::endl;
	Stack<float> s2{ 10 };
	s2.Push(10e-5);
	s2.Push(6e-3);
	s2.Push(14e0);
	s2.Push(93e-7);

	std::cout << *s2.Pop() << std::endl;
	std::cout << *s2.Pop() << std::endl;
	std::cout << *s2.Pop() << std::endl;
	std::cout << *s2.Pop() << std::endl;
}